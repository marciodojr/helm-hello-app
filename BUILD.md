## APP

```sh
docker build -f docker/api/Dockerfile -t hello-api .
```

### Executar:

```sh
docker run -d --rm --name hello-app -p 3000:3000 hello-app
docker logs -f hello-app
```

### Acessar

```
docker exec -it hello-app bash
```

### Remover

```
docker container rm -f hello-app
```

### Build

```sh
GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY=`cat ../gke-gitlab-11111-8927612a50c7.json`
CI_COMMIT_REF_NAME=`git rev-parse --abbrev-ref HEAD`
CI_COMMIT_SHA=`git rev-parse HEAD`
gitlab-runner exec docker build-app \
    --docker-privileged \
    --env="CI_COMMIT_SHA=${CI_COMMIT_SHA}" \
    --env="CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME}" \
    --env="GCP_PROJECT_ID=gke-gitlab-11111" \
    --env="API_URL=https://api.helm-example.mdojr.com.br" \
    --env="APP_URL=helm-example.mdojr.com.br" \
    --env="GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY=${GOOGLE_CONTAINER_REGISTRY_SERVICE_KEY}"
```